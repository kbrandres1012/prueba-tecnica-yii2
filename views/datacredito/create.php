<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Datacredito */

$this->title = 'Create Datacredito';
$this->params['breadcrumbs'][] = ['label' => 'Datacreditos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="datacredito-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
