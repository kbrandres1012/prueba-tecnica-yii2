<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $fullname
 * @property string $typeid
 * @property int $dni
 * @property string $email
 * @property string $phone
 * @property int $status
 *
 * @property Company[] $companies
 * @property Datacredito[] $datacreditos
 * @property User[] $users
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'typeid', 'dni', 'email', 'phone', 'status'], 'required'],
            [['dni', 'status'], 'integer'],
            [['fullname', 'typeid', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'typeid' => 'Typeid',
            'dni' => 'Dni',
            'email' => 'Email',
            'phone' => 'Phone',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatacreditos()
    {
        return $this->hasMany(Datacredito::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['customer_id' => 'id']);
    }
}
