<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $roles
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 * @property int $customer_id
 *
 * @property Customer $customer
 */
class Login extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'username', 'roles', 'password', 'authKey', 'accessToken', 'customer_id'], 'required'],
            [['id', 'customer_id'], 'integer'],
            [['roles'], 'string'],
            [['username'], 'string', 'max' => 180],
            [['password'], 'string', 'max' => 255],
            [['authKey', 'accessToken'], 'string', 'max' => 50],
            [['username'], 'unique'],
            [['id'], 'unique'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'roles' => 'Roles',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'customer_id' => 'Customer ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    public static function isUserAdmin($id){
        if(Login::findOne(['id'=>$id, 'roles' => 'ADMIN'])){
            return true;
        }else{
            return false;
        }
    }
    
    public static function isUserCustomer($id){
        if(Login::findOne(['id'=>$id, 'roles' => 'USER'])){
            return true;
        }else{
            return false;
        }
        
    }

    public static function isUserAnonymous($id){
        if($id == ''){
            return true;
        }else{
            return false;
        }
    }
}
